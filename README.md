
# My Portfolio

# TextEditor (Renamed to undisclose service name)
TextEditor is a multi-platform rich text editor SDK currently in use across various Naver services. It facilitates the creation of rich documents by offering variety of text styles (fonts, colors, sizes, etc.), as well as various components (images, videos, maps, etc.) and features(translation, spell checks) that assist users in writing.

<p align="center">
 <img src="https://gitlab.com/yankee_rin/portfolio/-/raw/main/assets/se-1.png?ref_type=heads", width="200"/>
<img src="https://gitlab.com/yankee_rin/portfolio/-/raw/main/assets/se-3.png?ref_type=heads", width="200"/>
<img src="https://gitlab.com/yankee_rin/portfolio/-/raw/main/assets/se-5.png?ref_type=heads", width="200"/>
</p>

**My Contribution**
- Involved in overall core design structure of the SDK. 
    - These include text editing(applying/resetting styles) structure, component placement, document publishing etc.
    - Developed while closely following Android, Web counterpart.
- Some of the key UI components
    - Toolbar
    - Map, Image, Video, Quotation and more
- Features
    - Location/Material search, translation and more
    - Image Caching
    - Collaborative Editing - multiple users from multiple platforms editing on a single document
- Service interface design
    - Delegating or delivering events from SDK to Services and vice versa
    - Service customization support
- Troubleshooting and maintenance

**Tech Used**
- Objective-C -> Swift
- MVC, MVVM
- Combine, Concurrency
- Carthage, SPM

# PhotoGenie
PhotoGenie is an SDK for photo decoration, allowing users to choose template photos and attach text, images, emojis, drawing elements, and more according to their preferences. The SDK supports transformation such as roation, translation, and scaling of each attachment via user gesture. PhotoGenie is currently provided within the Weverse app as a FanLetter feature.

<p align="center">
 <img src="https://gitlab.com/yankee_rin/portfolio/-/raw/main/assets/pg-1.gif?ref_type=heads", width="200"/>
<img src="https://gitlab.com/yankee_rin/portfolio/-/raw/main/assets/pg-2.gif?ref_type=heads", width="200"/>
<img src="https://gitlab.com/yankee_rin/portfolio/-/raw/main/assets/pg3.gif?ref_type=heads", width="200"/>
</p>

**My Contribution**
- As the only iOS developer for the SDK, I was responsible for building the app from scratch.
    - Core design structure and every UI element.
- Worked closely with Android counterpart and Weverse team members.

**Tech Used**
- Swift
- MVVM
- Combine, Concurrency, Core Data, Compositional Layout
- Carthage, SPM

# TextEditor for Article 
TextEditor for Article is a variation of SmartEditor SDK which supports theme based editing. Users can choose from various themes provided by the SDK to create documents that suit their preferences. This specific SDK was previously applied to the now-discontinued Article service.
<p align="center">
 <img src="https://gitlab.com/yankee_rin/portfolio/-/raw/main/assets/article-1.gif", width="200"/>
</p>

**My Contribution**
- Involved in theme structure design
    - JSON structure for each theme 
    - Theme loading, switching 
- Responsible for developing specific components for each theme

**Tech Used**
- Swift
- MVC, MVVM
- Carthage